package ru.gibdd.test.barsukovv.banklist.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import ru.gibdd.test.barsukovv.banklist.util.Constants;

/**
 * Класс для работы с БД. Отвечает за создание БД и за получение ссылок на DAO объекты
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "banks.db";
    private static final int DATABASE_VERSION = 1;

    private BankDao bankDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Bank.class);
        } catch (SQLException e) {
            Log.e(Constants.TAG, "Error creating database " + DATABASE_NAME + ". Error: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Bank.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Log.e(Constants.TAG, "error upgrading db " + DATABASE_NAME + " from version " + oldVersion);
            throw new RuntimeException(e);
        }
    }

    public BankDao getBankDao() throws SQLException {
        if (bankDao == null) {
            bankDao = new BankDao(getConnectionSource(), Bank.class);
        }

        return bankDao;
    }

    @Override
    public void close() {
        super.close();
        bankDao = null;
    }
}
