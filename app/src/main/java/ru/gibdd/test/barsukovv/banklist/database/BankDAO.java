package ru.gibdd.test.barsukovv.banklist.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

public class BankDao extends BaseDaoImpl<Bank, Integer> {

    protected BankDao(ConnectionSource connectionSource, Class<Bank> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Bank> getAllBanks() throws SQLException {
        return queryForAll();
    }
}
