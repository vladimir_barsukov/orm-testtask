package ru.gibdd.test.barsukovv.banklist.util;

import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import ru.gibdd.test.barsukovv.banklist.database.Bank;
import ru.gibdd.test.barsukovv.banklist.database.BankDao;
import ru.gibdd.test.barsukovv.banklist.database.DatabaseHelper;
import ru.gibdd.test.barsukovv.banklist.database.HelperFactory;

public class Utils {

    public static void saveDataToDatabase(List<Bank> bankList) {
        DatabaseHelper helper = HelperFactory.getHelper();
        try {
            BankDao bankDao = helper.getBankDao();
            for (Bank bank : bankList) {
                bankDao.createOrUpdate(bank);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error creating or updating records " + e);
        }
    }

    public static List<Bank> getBankListFromDatabase() {
        List<Bank> bankList;
        DatabaseHelper helper = HelperFactory.getHelper();
        try {
            BankDao bankDao = helper.getBankDao();
            bankList = bankDao.getAllBanks();
        } catch (SQLException e) {
            throw new RuntimeException("Error getting records " + e);
        }

        return bankList;
    }

    public static List<Bank> searchBanks(String searchText) {
        List<Bank> bankList;
        DatabaseHelper helper = HelperFactory.getHelper();
        try {
            BankDao bankDao = helper.getBankDao();
            QueryBuilder<Bank, Integer> builder = bankDao.queryBuilder();
            builder.where().like(Constants.COLUMN_NAME_BIK, "%" + searchText.trim() + "%").or().like(Constants.COLUMN_NAME_NAME, "%" + searchText.trim() + "%");
            PreparedQuery<Bank> preparedQuery = builder.prepare();
            bankList = bankDao.query(preparedQuery);
        } catch (SQLException e) {
            throw new RuntimeException("Error searching records " + e);
        }

        return bankList;
    }

    public static void saveBankDescription(String bik, String description) {
        DatabaseHelper helper = HelperFactory.getHelper();
        try {
            BankDao bankDao = helper.getBankDao();
            List<Bank> bankList = bankDao.queryForEq(Constants.COLUMN_NAME_BIK, bik);
            if (bankList != null && !bankList.isEmpty()) {
                Bank bank = bankList.get(0);
                bank.setDescription(description);
                bankDao.update(bank);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error updating records " + e);
        }
    }
}
