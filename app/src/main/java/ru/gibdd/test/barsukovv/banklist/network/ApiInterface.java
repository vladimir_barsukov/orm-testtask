package ru.gibdd.test.barsukovv.banklist.network;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import ru.gibdd.test.barsukovv.banklist.util.Banks;
import ru.gibdd.test.barsukovv.banklist.util.Constants;
import ru.gibdd.test.barsukovv.banklist.util.Description;

public interface ApiInterface {

    @Headers({Constants.HEADER_CONTENT_TYPE, Constants.HEADER_PROJECT_KEY, Constants.HEADER_USER_KEY})
    @GET("getBanks")
    Call<Banks> getBanks();

    @Headers({Constants.HEADER_CONTENT_TYPE, Constants.HEADER_PROJECT_KEY, Constants.HEADER_USER_KEY})
    @GET("getDescription/{bik}")
    Call<Description> getDescription(@Path("bik") String bik);

    @Headers({Constants.HEADER_CONTENT_TYPE, Constants.HEADER_PROJECT_KEY, Constants.HEADER_USER_KEY})
    @GET("getBanksWithDescription")
    Call<Banks> getBanksWithDescription();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
