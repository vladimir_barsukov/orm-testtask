package ru.gibdd.test.barsukovv.banklist;

import android.app.Application;

import ru.gibdd.test.barsukovv.banklist.database.HelperFactory;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());
    }
    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }

}
