package ru.gibdd.test.barsukovv.banklist.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.gibdd.test.barsukovv.banklist.R;
import ru.gibdd.test.barsukovv.banklist.database.Bank;
import ru.gibdd.test.barsukovv.banklist.database.BankDao;
import ru.gibdd.test.barsukovv.banklist.database.DatabaseHelper;
import ru.gibdd.test.barsukovv.banklist.database.HelperFactory;
import ru.gibdd.test.barsukovv.banklist.network.ApiInterface;
import ru.gibdd.test.barsukovv.banklist.util.Constants;
import ru.gibdd.test.barsukovv.banklist.util.Description;
import ru.gibdd.test.barsukovv.banklist.util.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class BankDetailsFragment extends Fragment implements Callback<Description> {

    private static String mBik;

    @BindView(R.id.description)
    TextView descriptionView;

    public static BankDetailsFragment newInstance(int position, String bik) {
        BankDetailsFragment detailsFragment = new BankDetailsFragment();
        mBik = bik;

        Bundle args = new Bundle();
        args.putInt(Constants.KEY_POSITION, position);
        args.putString(Constants.KEY_BIK, bik);
        detailsFragment.setArguments(args);

        return detailsFragment;
    }

    public int getShownIndex() {
        return getArguments().getInt(Constants.KEY_POSITION, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bank_details, container, false);
        ButterKnife.bind(this, view);

        mBik = getArguments().getString(Constants.KEY_BIK);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        showDescription(mBik);
    }

    private void showDescription(String bik) {
        DatabaseHelper helper = HelperFactory.getHelper();
        try {
            BankDao bankDao = helper.getBankDao();
            List<Bank> queryList = bankDao.queryForEq(Constants.COLUMN_NAME_BIK, bik);
            if (queryList != null && !queryList.isEmpty()) {
                Bank bank = queryList.get(0);
                Log.d(Constants.TAG, "Description " + queryList.get(0));
                if (bank != null && bank.getDescription() != null) {
                    descriptionView.setText(bank.getDescription());
                } else {
                    // download description
                    getDescription(bik);
                }
            } else {
                // download description
                getDescription(bik);
            }
        } catch (SQLException e) {
            Log.e(Constants.TAG, "Error getting BankDao object with bik " + bik);
            throw new RuntimeException(e);
        }
    }

    private void getDescription(String bik) {
        ApiInterface apiInterface = ApiInterface.retrofit.create(ApiInterface.class);
        Call<Description> call = apiInterface.getDescription(bik);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Description> call, Response<Description> response) {
        Description description = response.body();
        String descriptionStr = description.getDescription();

        if (descriptionStr != null && !descriptionStr.isEmpty()) {
            Utils.saveBankDescription(mBik, descriptionStr);
            descriptionView.setText(descriptionStr);
        } else {
            descriptionView.setText(getString(R.string.description_not_found_message));
        }
    }

    @Override
    public void onFailure(Call<Description> call, Throwable t) {
        descriptionView.setText(getString(R.string.description_not_found_message));
        Log.d(Constants.TAG, "Error getting bank description" + t.getMessage() + " " + t.getCause());
    }
}
