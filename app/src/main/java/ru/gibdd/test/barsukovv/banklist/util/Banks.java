package ru.gibdd.test.barsukovv.banklist.util;

import java.util.List;

import ru.gibdd.test.barsukovv.banklist.database.Bank;

/**
 * Класс предназначен для конвертации списка банков из JSON - массива в список
 */
public class Banks {
    public List<Bank> banks;

    public List<Bank> getBanks() {
        return banks;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }
}
