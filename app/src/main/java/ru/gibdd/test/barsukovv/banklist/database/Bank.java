package ru.gibdd.test.barsukovv.banklist.database;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import ru.gibdd.test.barsukovv.banklist.util.Constants;

/**
 * Класс-модель для хранения данных о банках
 */
@DatabaseTable(tableName = "banks")
public class Bank {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.STRING, columnName = Constants.COLUMN_NAME_NAME)
    private String name;

    @DatabaseField(dataType = DataType.STRING, columnName = Constants.COLUMN_NAME_BIK)
    private String bik;

    @DatabaseField(dataType = DataType.STRING)
    private String description;

    public Bank() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBik() {
        return bik;
    }

    public void setBik(String bik) {
        this.bik = bik;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Bank {name: " + name + ", bik: " + bik + ", description: " + description + "}";
    }
}
