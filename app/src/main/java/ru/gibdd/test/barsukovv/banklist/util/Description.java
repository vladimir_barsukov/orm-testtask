package ru.gibdd.test.barsukovv.banklist.util;

/**
 * Класс предназначен для конвертации описания банка из JSON - объекта в строку
 */
public class Description {

    public String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
