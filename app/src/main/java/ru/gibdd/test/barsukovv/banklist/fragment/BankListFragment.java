package ru.gibdd.test.barsukovv.banklist.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.gibdd.test.barsukovv.banklist.DetailsActivity;
import ru.gibdd.test.barsukovv.banklist.R;
import ru.gibdd.test.barsukovv.banklist.adapter.CustomArrayAdapter;
import ru.gibdd.test.barsukovv.banklist.database.Bank;
import ru.gibdd.test.barsukovv.banklist.network.ApiInterface;
import ru.gibdd.test.barsukovv.banklist.util.Banks;
import ru.gibdd.test.barsukovv.banklist.util.Constants;
import ru.gibdd.test.barsukovv.banklist.util.Utils;

/**
 * Фрагмент содержит список банков.
 */
public class BankListFragment extends ListFragment implements Callback<Banks> {

    private boolean mDualPane;
    private int mCurrentPosition = 0;
    private String mBik = "";

    public BankListFragment() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);
        loadBankList();

        View detailsFragment = getActivity().findViewById(R.id.details);
        mDualPane = detailsFragment != null && detailsFragment.getVisibility() == View.VISIBLE;

        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(Constants.KEY_CURRENT_POSITION, 0);
            mBik = savedInstanceState.getString(Constants.KEY_BIK, "");
        }

        if (mDualPane) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.KEY_CURRENT_POSITION, mCurrentPosition);
        outState.putString(Constants.KEY_BIK, mBik);
    }

    /**
     * Показать детальное описание банка
     *
     * @param position выбранная позиция в списке банков
     */
    private void showDetails(int position, String bik) {
        mCurrentPosition = position;

        if (mDualPane) {
            getListView().setItemChecked(position, true);

            BankDetailsFragment details = (BankDetailsFragment) getFragmentManager().findFragmentById(R.id.details);
            if (details == null || details.getShownIndex() != position) {
                details = BankDetailsFragment.newInstance(position, bik);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.details, details);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }

        } else {
            Intent intent = new Intent();
            intent.setClass(getActivity(), DetailsActivity.class);
            intent.putExtra(Constants.KEY_POSITION, position);
            intent.putExtra(Constants.KEY_BIK, bik);
            startActivity(intent);
        }
    }

    @Override
    public void onListItemClick(ListView listView, View v, int position, long id) {
        Bank selectedBank = (Bank) listView.getItemAtPosition(position);
        mBik = selectedBank != null ? selectedBank.getBik() : "";

        showDetails(position, mBik);

    }

    private void setupListAdapter(List<Bank> bankList) {
        CustomArrayAdapter<Bank> adapter = new CustomArrayAdapter<Bank>(getActivity(), android.R.layout.simple_list_item_2, bankList);
        setListAdapter(adapter);
    }

    private void loadBankList() {
        ApiInterface apiInterface = ApiInterface.retrofit.create(ApiInterface.class);
        Call<Banks> call = apiInterface.getBanks();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Banks> call, Response<Banks> response) {
        Banks banks = response.body();

        List<Bank> bankList = banks.getBanks();
        Utils.saveDataToDatabase(bankList);

        setupListAdapter(bankList);
    }

    @Override
    public void onFailure(Call<Banks> call, Throwable t) {
        // Не удалось получить список банков. пытаемся загрузить его из кэша
        Log.e(Constants.TAG, "Error " + t.getMessage() + " " + t.getCause());
        List<Bank> bankList = Utils.getBankListFromDatabase();
        setupListAdapter(bankList);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.search);

        SearchView sv = new SearchView(getActivity());
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String searchText) {
                if (searchText.trim().length() <= Constants.SEARCH_QUERY_LENGTH) {
                    Toast.makeText(getActivity(), getString(R.string.search_message), Toast.LENGTH_LONG).show();
                    return true;
                } else {
                    doSearch(searchText);
                    return false;
                }
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Если пользователь очистил строку поиска, отобразить весь список банков
                if (newText.isEmpty()) {
                    resetSearchQuery();
                }
                return true;
            }
        });
        item.setActionView(sv);
    }

    private void resetSearchQuery() {
        List<Bank> bankList = Utils.getBankListFromDatabase();
        setupListAdapter(bankList);
    }

    private void doSearch(String searchText) {
        List<Bank> bankList = Utils.searchBanks(searchText);
        setupListAdapter(bankList);
    }

}
