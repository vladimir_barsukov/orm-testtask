package ru.gibdd.test.barsukovv.banklist.util;

/**
 * Содержит константы используемые в приложении
 */
public class Constants {
    public static final String TAG = "gibddtesttask";
    public static final String KEY_POSITION = "position";
    public static final String KEY_BIK = "bik";
    public static final String KEY_CURRENT_POSITION = "currentPosition";

    public static final String BASE_URL = "http://jsonstub.com/";

    public static final String HEADER_CONTENT_TYPE = "Content-type: application/json";
    public static final String HEADER_PROJECT_KEY = "JsonStub-Project-Key: e793c241-7a07-4b1f-adac-eebc62013306";
    public static final String HEADER_USER_KEY = "JsonStub-User-Key: 3eb99fcb-379f-49b9-9358-55ecc358dfa5";

    public static final String COLUMN_NAME_BIK = "bik";
    public static final String COLUMN_NAME_NAME = "name";

    public static final int SEARCH_QUERY_LENGTH = 2;
}
