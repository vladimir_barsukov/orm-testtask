package ru.gibdd.test.barsukovv.banklist.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import ru.gibdd.test.barsukovv.banklist.database.Bank;

public class CustomArrayAdapter<B> extends ArrayAdapter<Bank> {

    private Context mContext;
    private int mResource;
    private List<Bank> mObjects;

    public CustomArrayAdapter(Context context, int resource, List<Bank> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        this.mObjects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(mResource, parent, false);

        TextView bankName = (TextView) rowView.findViewById(android.R.id.text1);
        TextView bankBik = (TextView) rowView.findViewById(android.R.id.text2);

        bankName.setText(mObjects.get(position).getName());
        bankBik.setText(mObjects.get(position).getBik());

        return rowView;
    }

}
