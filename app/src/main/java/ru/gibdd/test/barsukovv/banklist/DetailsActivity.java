package ru.gibdd.test.barsukovv.banklist;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import ru.gibdd.test.barsukovv.banklist.fragment.BankDetailsFragment;
import ru.gibdd.test.barsukovv.banklist.util.Constants;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        int position = getIntent().getIntExtra(Constants.KEY_CURRENT_POSITION, 0);
        String bik = getIntent().getStringExtra(Constants.KEY_BIK);

        Bundle arguments = new Bundle();
        arguments.putString(Constants.KEY_BIK, bik);
        arguments.putInt(Constants.KEY_CURRENT_POSITION, position);

        BankDetailsFragment detailsFragment = new BankDetailsFragment();
        detailsFragment.setArguments(arguments);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.detailsFrame, detailsFragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // ОБработка нажатия на кнопку Up/Home
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
